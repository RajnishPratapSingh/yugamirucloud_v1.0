﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class MuscleStateInfoElement
    {

        int m_iMuscleID;
        int m_iMuscleStatePatternID;
        int m_iMuscleStateTypeID;
        int m_iMuscleStateLevel;


        // ‹Ø“÷ó‘Ôî•ñ‚Ì—v‘f.
        public MuscleStateInfoElement()
        {
            m_iMuscleID = Constants.MUSCLEID_NONE;

            m_iMuscleStatePatternID = Constants.MUSCLESTATEPATTERNID_NONE;

            m_iMuscleStateTypeID = Constants.MUSCLESTATETYPEID_NONE;

    m_iMuscleStateLevel = 0;
      
        }

        public MuscleStateInfoElement( int iMuscleID, int iMuscleStatePatternID, int iMuscleStateTypeID, int iMuscleStateLevel )
        {

            m_iMuscleID = iMuscleID ;

            m_iMuscleStatePatternID = iMuscleStatePatternID;

            m_iMuscleStateTypeID = iMuscleStateTypeID ;

            m_iMuscleStateLevel = iMuscleStateLevel ;
        
        }

     
    public int GetMuscleID( ) 
{
	return m_iMuscleID;
}

public int GetMuscleStatePatternID() 
{
	return m_iMuscleStatePatternID;
}

        public int GetMuscleStateTypeID() 
{
	return m_iMuscleStateTypeID;
}

        public int GetMuscleStateLevel() 
{
	return m_iMuscleStateLevel;
}

    }
}
